<!--reutilizamos el layout que viene con bosstrap-->
@extends('layouts.app')

<!--reescribimos todo-->
@section('content')

		<form method="post" action="{{ route('producto.update', $producto->id) }}">
			<!--@csrf es  para indicar que es una peticion del navegador
				y no un acceso desde un link de fuente externa-->
				@csrf
				@method('PUT')
			<div class="container">
		  <div class="form-group">
			    <label for="txtNombre">Nombre</label>
			    <input type="text" class="form-control" name="txtNombre" id="txtNombre" placeholder="Ingrese su nombre" value="{{ $producto->nombre }}">
		  </div>
		  <div class="form-group">
			    <label for="txtPrecio">Precio</label>
			    <input type="number" class="form-control" name="txtPrecio" id="txtPrecio" placeholder="Ingrese el precio" value="{{ $producto->precio }}">
		  </div>

		  <!--Falta rescatar categoria-->
		  <div class="form-group">
			    <label for="cboCategoria">Categoria</label>
			    <select  id="cboCategoria" class="form-control" name="cboCategoria">
			    	<option value="">Seleccionar</option>
	    			<!--por cada categoria hacemos un optcion-->
	    			@foreach($categorias as $categoria)
	    			<!--el value es lo que se envia cada ves que el usuario pincha-->
	    			<!--dentro de option lo que se muestra al usuario la columna de la bd-->
	    				<option value="{{ $categoria->id }}" {{ ($producto->categoria->id == $categoria->id) ?'selected': ''}}>{{ $categoria->nombre}}</option>
	    			@endforeach
			    </select>
		  </div>
		   <div class="form-group">
			    <label for="txtStock">Stock</label>
			    <input type="number" class="form-control" name="txtStock" id="txtStock" placeholder="Ingrese el stock" value="{{ $producto->stock }}">
		  </div>

		  <input type="submit" name="btnModificar" id="btnModificar" class="btn btn-success" value="Modificar">
		  
		 </div>
		</form>	

@endsection