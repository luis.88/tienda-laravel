<!--reutilizamos el layout que viene con bosstrap-->
@extends('layouts.app')

<!--reescribimos todo-->
@section('content')


		<!--producto.store es el alias que se genera para 
			enviar el listado de productos-->
		<form method="post" action="{{ route('producto.store') }}">
			<!--@csrf es  para indicar que es una peticion del navegador
				y no un acceso desde un link de fuente externa-->
				@csrf
			<div class="container">
		  <div class="form-group">
			    <label for="txtNombre">Nombre</label>
			    		<!--con el value  old cada vez que guardamos queda seteado el ingreso que hicimos anteriormente
			    			con el old traemos el id que existia antes-->
			    <input type="text" class="form-control" name="txtNombre" id="txtNombre" placeholder="Ingrese su nombre" value="{{ old('txtNombre') }}">
		  </div>
		  <div class="form-group">
			    <label for="txtPrecio">Precio</label>
			    <input type="number" class="form-control" name="txtPrecio" id="txtPrecio" placeholder="Ingrese el precio">
		  </div>
		  <div class="form-group">
			    <label for="cboCategoria">Categoria</label>
			    <select  id="cboCategoria" class="form-control" name="cboCategoria">
			    	<option value="">Seleccionar</option>
	    			<!--por cada categoria hacemos un optcion-->
	    			@foreach($categorias as $categoria)
	    			<!--el value es lo que se envia cada ves que el usuario pincha-->
	    			<!--dentro de option lo que se muestra al usuario la columna de la bd-->
	    				<option value="{{ $categoria->id }}">{{ $categoria->nombre}}</option>
	    			@endforeach
			    </select>
		  </div>
		   <div class="form-group">
			    <label for="txtStock">Stock</label>
			    <input type="number" class="form-control" name="txtStock" id="txtStock" placeholder="Ingrese el stock">
		  </div>
			
		<!--campo date simulado , solo para probar un tipo date,no esta creado en bd
  		   <div class="form-group">
			    <label for="txtFecha">Date</label>
			    <input type="date" class="form-control" name="txtFecha" id="txtFecha" placeholder="Ingrese el stock">
		  </div>
		  -->

		  <input type="submit" name="btnGuardar" id="btnGuardar" class="btn btn-success" value="Guardar">
		  
		 </div>
		</form>	

@endsection