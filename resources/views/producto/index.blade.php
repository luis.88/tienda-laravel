<!--reutilizamos el layout que viene con bosstrap-->
@extends('layouts.app')

<!--reescribimos todo-->
@section('content')



		<h2>Listado de productos</h2>

		<table class="table table-striped">
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Precio</th>
				<th>Stock</th>
				<th>Categoria</th>
				<!--por cada fila que imprima existira un boton eliminar-->
				<th>Herramientas</th>
			</tr>
		<!--
		con foreach recorremos la lista,
		por cada ciclo generamos un tr
		-->
		@foreach($productos as $producto)
			<tr>
				<td>{{ $producto->id}}</td>
				<td>{{ $producto->nombre}}</td>
				<td>{{ $producto->precio}}</td>
				<td>{{ $producto->stock}}</td>
				<!--aca rescatamo el objeto categoria y desde ahi
					acceder a un atributo especifico de esta-->
				<td>{{ $producto->categoria->nombre}}</td>

				<td>
				<!--indicamos el alias producto.destroy y el id del producto a eliminar-->
					<form method="post" action="{{ route('producto.destroy', $producto->id)}}">
						<!--indicamos el metodo que entrega laravel-->
						@method('DELETE')
						<!--token de seguridad,peticion desde el navegador del usuario-->
						@csrf
						<input type="submit" name="btnEliminar" value="Eliminar">
					</form>
					<a href="{{ route('producto.edit', $producto->id) }}">Modificar</a>
				</td>
			</tr>
		@endforeach

		</table>
		<!--si en la session hay una variable mensaje
			si es asi imprimimos esa variable-->
		@if(session('mensaje'))
			{{ session('mensaje')}}
		@endif

@endsection