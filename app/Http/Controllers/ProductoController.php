<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//importar modelos
use App\Producto;
use App\Categoria;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //codigo para listas las cosas
    public function index()
    {
        //rescatar todos los productos que existen en la base de datos
        //los entrega en una lista automaticamente
        $productos = Producto::all();

        // ahora envio los datos a la vista
        // con with envio los productos
        return view('producto.index')->with('productos', $productos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    // agregar un nuevo producto
    public function create()
    {
        //traemos las categorias que hay en la base de datos
        $categorias = Categoria::all();
        //mostrar el formulario vacio
        //producto.create = es el nombre de la vista
        //con esto dejamos disponible en la vista create las categorias de la bd
        return view('producto.create')->with('categorias',$categorias);
    }


    //creamos funcion para reutilizar las validacione
    public function validar(Request $request){
        //validador laravel
        //le decimos lo que queremos validar y le pasamos un array
        $this->validate($request, [
            //cuando es varchar el min se refiere a caracteres
            //unique significa que ese nombre es unico en la tabla productos y en el campo nombre
           'txtNombre' => 'required|min:3|max:20|unique:productos,nombre',
           //cuando es numeric el min se refiere a numeros
           'txtStock' => 'required|numeric|min:1',
           //exist obliga que debe existir esa categoria
           // dentro de la tabla categoria con ese id
           'cboCategoria' => 'required|exists:categorias,id',
           'txtPrecio' => 'required|numeric|min:490'

           //ejemplo de validacion fecha tipo date
           //aqui valida que la fecha sea antes del 18-02-2019
           //podriamos ocupar before:today o after:today
           //'txtFecha' => 'required|date|before:18-02-2019'
           //email
        ], [
            'txtNombre.required' => 'Campo nombre requerido',
            'txtStock.required' => 'Campo stock requerido',
            'txtNombre.min' => 'El nombre debe tener minimo 3 letras',
            'txtNombre.unique' => 'ya existe un producto con ese nombre',
            'txtStock.numeric' => 'el stock debe ser numerico'

        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    //guardar los productos en la base de datos
    public function store(Request $request)
    {

        //llamamos a la funcion para validar
        $this->validar($request);
        //creamos un producto
        $producto = new Producto();
        //rescatamos los datos que van llegando del formulario
        // a traves del input
        $producto->nombre = $request->get('txtNombre');
        $producto->stock = $request->get('txtStock');
        $producto->precio = $request->get('txtPrecio');
        $producto->categoria_id = $request->get('cboCategoria');

        //mensaje de error
        $mensaje = "Error al guardar";
        // preguntamos si podemos guardar esto
        //a traves del metodo save guardamos en la bd
        if ($producto->save()) {
           $mensaje = "Guardado correctamente";
        }

        // sin es guardado correctamente retornamos la lista
        //utilizamods el alias producto.index y con un mensaje guardado en la sesion
        return redirect()->route('producto.index')->with('mensaje', $mensaje);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // muestra los productos
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //muestra un producto para editar
    public function edit($id)
    {
       $producto = Producto::find($id);

       if ($producto == null) {
           redirect()->route('producto.index')
                    ->with('mensaje',"Producto ya no existe");
       }

       //mandamos a la vista el producto 
       return view('producto.edit')->with('producto', $producto)
         //le mandamos las categorias para que pueda mostrarlas en el combox   
            ->with('categorias', Categoria::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // guarda el producto que se esta editando
    public function update(Request $request, $id)
    {
       $producto = Producto::find($id);

       if ($producto == null) {
           redirect()->route('producto.index')
                    ->with('mensaje',"Producto ya no existe");
       }

       //si ya existe recogemos los datos que vienen del formulario
        $producto->nombre = $request->get('txtNombre');
        $producto->stock = $request->get('txtStock');
        $producto->precio = $request->get('txtPrecio');
        $producto->categoria_id = $request->get('cboCategoria');

        //mensaje de error
        $mensaje = "Error al modificar";
        // preguntamos si podemos guardar esto
        //a traves del metodo save guardamos en la bd
        if ($producto->save()) {
           $mensaje = "Modificado correctamente";
        }

        // sin es guardado correctamente retornamos la lista
        //utilizamods el alias producto.index y con un mensaje guardado en la sesion
        return redirect()->route('producto.index')->with('mensaje', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // elimina un producto
    // inyectamos automaticamente el id del producto que se va a eliminar
    public function destroy($id)
    {
        //obtenemos el producto, llamando al modelo,y mediante el metodo find()buscamos el producto
        $producto = Producto::find($id);

        //validaciones
        // si el producto ya no existe le enviamos un mensaje
        if ($producto==null) {
            return redirect()->route('producto.index')
                ->with('mensaje', "el producto ya no existe");
        }

        //antes de eliminar preguntamos si esto esta en alguna venta
        // me devuelve un arreglo de todas las ventas
        if (count($producto->ventas) > 0) {
            return redirect()->route('producto.index')
                ->with('mensaje', "Este producto ya tiene ventas asociadas");
        }

        //eliminamos un producto
        $producto->delete();

            return redirect()->route('producto.index')
                ->with('mensaje', "eliminado correctamente");

    }
}
