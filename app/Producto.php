<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//el modelo esta vacion , pero se conecta a la tabla productos
class Producto extends Model
{	

	// para desactivar las funcionalidad que pide obligatoriamente las columnas
	//timestamp es para mantener un log de cuando se crea un registro 
	//public $timestamp = false;

    // indicamos como se une la tabla producto con categoria
	//unimos las 2 tablas , categoria con producto
	// pasa a ser una especie de join
    //metodo categoria
    public function categoria(){
    	//este producto pertenece a una categoria
    	return $this->belongsTo('App\Categoria');
    }


    // es ventas , porque el producto puede estar en mas de una venta
    //de esta forma laravel se conecta 
        public function ventas(){
    	//belongsToMany pertenece a muchas ventas
    	return $this->belongsToMany('App\Venta');
    }


}
