<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



// crear ruta personalizada
// cuando se ingresa a producto/listado ,
// se ejecuta el controlador ProductoController
// y dentro de este se ejecuta la funcion index
//Route::get('/producto/listado', 'ProductoController@index');


// con el middleware hacemos que antes de cada peticion llame a un componente llamado auth y que va autenticar un grupo de rutas
Route::middleware('auth')->group(function(){
	//todas las rutas que esten dentro de esta funcion va a estar autenticadas
	// crear todas las rutas para ProductoController 
	Route::resource('producto', 'ProductoController');
});



Auth::routes();

//aca podemos indicar hacia donde queremo que se rediriga una ves que ya se ha loguedao el usuario
Route::get('/home', 'HomeController@index')->name('home');
